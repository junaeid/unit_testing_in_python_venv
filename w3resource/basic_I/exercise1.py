def print_hello_world():
    return "Hello World"


def test_print_hello_world():
    """testcase of Hello world"""
    value = print_hello_world()
    assert "Hello World" == value
