"""
Exercise Question 9: Display -10 to -1 using for loop

Expected output:

-10
-9
-8
-7
-6
-5
-4
-3
-2
-1
"""


def display_negative_to_positive_number():
    list = []
    for i in range(-10, 0):
        print(i)
        list.append(i)
    return list


def test_display_negative_to_positive_number():
    result = [-10, -9, -8, -7, -6, -5, -4, -3, -2, -1]
    assert result == display_negative_to_positive_number()
