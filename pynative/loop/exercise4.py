"""
Exercise Question 4: Accept n number from user and print its multiplication table
"""
"""
output:
2
4
6
8
10
12
14
16
18
20
"""

import pytest


def n_numbers_multiplication_table(number):
    result = []
    for i in range(11):
        result.append(i * number)
    return result


def test_n_numbers_multiplication_table():
    number = 2
    result = [0, 2, 4, 6, 8, 10, 12, 14, 16, 18, 20]
    assert n_numbers_multiplication_table(number) == result
