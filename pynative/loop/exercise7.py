"""
Exercise Question 7: Print the following pattern using for loop

5 4 3 2 1
4 3 2 1
3 2 1
2 1
1
"""


def display_n_number_pyramid_reverse_order(number):
    result = []
    k = number
    for i in range(0, number + 1):
        for j in range(k - i, 0, -1):
            result.append(j)
            print(j, end=" ")
        print("\n")
    return result


def test_display_n_number_pyramid_reverse_order():
    number = 5
    result = [5, 4, 3, 2, 1, 4, 3, 2, 1, 3, 2, 1, 2, 1, 1]
    assert result == display_n_number_pyramid_reverse_order(number)
