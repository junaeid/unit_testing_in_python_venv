"""
Exercise Question 5: Given a list iterate it and display numbers which are divisible by 5 and if you find number greater than 120 stop the loop iteration

list1 = [12, 15, 32, 42, 55, 75, 122, 132, 150, 180, 200]

Expected output:

15
55
75
150
"""


def display_numbers_divisible_by_5(list):
    result = []
    for i in list:
        if i > 150:
            break
        if i % 5 == 0:
            result.append(i)
    return result


def test_display_numbers_divisible_by_5():
    list = [12, 15, 32, 42, 55, 75, 122, 132, 150, 180, 200]
    output = [15, 55, 75, 150]
    result = display_numbers_divisible_by_5(list)
    assert output == result
