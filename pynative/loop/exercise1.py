import pytest


def natural_loop(x):
    y = []
    for i in range(1, x):
        y.append(i)
    return y


def test_natural_loop():
    x = 11
    y = []
    for i in range(1, x):
        y.append(i)
    assert natural_loop(x) == y
