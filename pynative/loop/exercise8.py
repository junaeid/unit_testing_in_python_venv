"""
Exercise Question 8: Reverse the following list using for loop

list1 = [10, 20, 30, 40, 50]

Expected output:

50
40
30
20
10
"""


def reverse_list_using_forloop(list1):
    list1 = [10, 20, 30, 40, 50]
    list2 = []
    start = len(list1) - 1
    stop = -1
    step = -1
    for i in range(start, stop, step):
        print(list1[i])
        list2.append(list1[i])
    return list2


def test_reverse_list_using_forloop():
    list1 =  [50, 40, 30, 20, 10]
    assert reverse_list_using_forloop(list1) == list1
