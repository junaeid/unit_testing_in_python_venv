"""Exercise Question 2: Print the following pattern"""
"""
1 
1 2 
1 2 3 
1 2 3 4 
1 2 3 4 5
"""


def left_aline_pyramid(x):
    x = 6
    y = []
    for i in range(1, x):
        for j in range(1, i + 1):
            y.append(j)
            print(j, end=" ")
        print(" ")
    return y


def test_left_aline_pyramid():
    x = 6
    y = left_aline_pyramid(x)
    z = [1, 1, 2, 1, 2, 3, 1, 2, 3, 4, 1, 2, 3, 4, 5]
    assert left_aline_pyramid(x) == z
