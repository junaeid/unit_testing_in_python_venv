"""
Exercise Question 3: Accept n number from user and calculate the sum of all number between 1 and n including ns
input 10
output 55
"""


def calculate_the_sum_of_all_number(x):
    sum = 0
    for i in range(x + 1):
        sum += i
    return sum


def test_calculate_the_sum_of_all_number():
    result = calculate_the_sum_of_all_number(10)
    assert result == 55
