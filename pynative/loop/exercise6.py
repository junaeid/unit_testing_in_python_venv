"""
Exercise Question 6: Given a number count the total number of digits in a number

For example, the number is 75869, so the output should be 5.
"""


def count_the_total_number_of_digits_in_a_number(number):
    count = 0
    while number != 0:
        number //= 10
        count += 1
    return count


def test_count_the_total_number_of_digits_in_a_number():
    number = 75869
    result = count_the_total_number_of_digits_in_a_number(number)
    assert result == 5
