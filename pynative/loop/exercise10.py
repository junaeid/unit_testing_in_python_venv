"""
Exercise Question 10: Display a message “Done” after successful execution of for loop

For example, the following loop will execute without any error.

for i in range(5):
    print(i)

So the Expected output should be:

0
1
2
3
4
Done!
"""


def display_a_message_end_of_the_for_loop():
    output = ""
    for i in range(5):
        print(i)
    output = "done"
    return output


def test_display_a_message_end_of_the_for_loop():
    result = "done"
    assert result == display_a_message_end_of_the_for_loop()
